## The Thin Client Configuration Rule File

### File structure
* The configuration of thin clients is controlled via the configuration file mapping the client MAC accress to a configuration line
* The file has to be a CSV formatted file named igel_config.csv
* Mandatory file header is MAC,Site,Function,Username,Printer
  - MAC: MAC address of the client without separators such as colon ':' or dash '-'
  - Site: Any value accepted
  - Function: one of the predefined functions
     - Advice
     - ASC
     - Guest
     - Catering
     - Medical
     - Ops
     - Shift
     - Staff
     - Transport
  - Username: CCGuest%number% (example CCGuest001) for Guest function, CCAdmin%number% (example CCAdmin001) for non Guest functions
  - Printer: Last octet of printer IP addresses separated by pilpe '|'. The first item will be set as default printer

### Primary file location
* During execution the configuration is loaded from the GitLab repository https://gitlab.com/bbiczo/aimartcpconfig by default
* Persons permissioned to the repositry can edit the file on the GitLab repository with the built-in text editor or use source contol tools such as SourceTree to make offline changes then commit it to the repository

### Fallback file location
* If the file from the primary location is not accessible it is loaded from the root of the USB drive from where the configuration was launched